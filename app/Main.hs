module Main where

import Date
import Plate
import Restriction
import Text.Read (readMaybe)
import Time

-- | Get from the user the time
getTime :: IO (Time)
getTime = do
  putStrLn "Please write the current Time (hh:mm:ss)"
  rawTime <- getLine
  case readTime rawTime of
    Left err -> do
      putStrLn err
      getTime
    Right x -> return x

-- | Get from the user the date
getDate :: IO (Date)
getDate = do
  putStrLn "Please write the current Date (mm-dd-yyyy)"
  rawTime <- getLine
  case readDate rawTime of
    Left err -> do
      putStrLn err
      getDate
    Right x -> return x

-- | Get from user the car's plate number
getPlateNumber :: IO (Int)
getPlateNumber = do
  putStrLn "Please write the car's plate number"
  rawPlateNumber <- getLine
  case readMaybe rawPlateNumber :: Maybe Int of
    Just x -> return x
    Nothing -> do
      putStrLn "parse error"
      getPlateNumber

-- | Print readable output
printPicoPlaca :: Bool -> IO ()
printPicoPlaca x =
  if x
    then putStrLn "The car is not allowed to be on the road"
    else putStrLn "The car is allowed to be on the road"

main :: IO ()
main = do
  plateNumber <- getPlateNumber
  date <- getDate
  time <- getTime
  printPicoPlaca $ checkPicoPlaca (lastDigit plateNumber) date time
  return ()
