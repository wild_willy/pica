module DateSpec
  ( spec
  ) where

import Date
import Test.Hspec

spec :: Spec
spec = do
  describe "readDate" $ do
    it "returns the proper date" $ do
      shouldBe (readDate "12-24-2019") (Right (Date 12 24 2019))
      shouldBe (readDate "11-10-2018") (Right (Date 11 10 2018))
    context "when provided with invalid input" $ do
      it "returns a parse error" $ do
        shouldBe (readDate "xx-xx-xxxx") (Left "parse error")
      it "returns not valid date" $ do
        shouldBe (readDate "13-09-1990") (Left "not valid date")
        shouldBe (readDate "02-31-2019") (Left "not valid date")
        shouldBe (readDate "01-33-2019") (Left "not valid date")
        shouldBe (readDate "03-32-2019") (Left "not valid date")
        shouldBe (readDate "04-31-2019") (Left "not valid date")
        shouldBe (readDate "05-32-2019") (Left "not valid date")
  describe "dateToDayWeek" $ do
    it "returns the proper day of the week" $ do
      shouldBe (dateToDayWeek $ Date 08 30 2019) Friday
      shouldBe (dateToDayWeek $ Date 10 13 2020) Tuesday
