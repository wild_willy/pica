module RestrictionSpec
  ( spec
  ) where

import Date
import Plate
import Restriction
import Test.Hspec
import Time

spec :: Spec
spec = do
  describe "checkPicoPlaca" $ do
    it "returns the proper value" $ do
      shouldBe (checkPicoPlaca One (Date 08 26 2019) (Time 08 30 00)) True
      shouldBe (checkPicoPlaca Two (Date 08 27 2019) (Time 08 30 00)) False
      shouldBe (checkPicoPlaca One (Date 08 26 2019) (Time 10 00 00)) False
      shouldBe (checkPicoPlaca Three (Date 08 27 2019) (Time 17 00 00)) True
      shouldBe (checkPicoPlaca Five (Date 08 28 2019) (Time 19 29 00)) True
