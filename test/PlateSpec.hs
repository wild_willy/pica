module PlateSpec
  ( spec
  ) where

import Plate
import Test.Hspec

spec :: Spec
spec = do
  describe "lastDigit" $ do
    it "returns the proper last digit" $ do
      shouldBe (lastDigit 23) Three
      shouldBe (lastDigit 42) Two
      shouldBe (lastDigit 1340) Zero
