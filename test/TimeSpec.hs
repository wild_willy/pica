module TimeSpec
  ( spec
  ) where

import Test.Hspec
import Time

spec :: Spec
spec = do
  describe "readTime" $ do
    it "returns the proper time" $ do
      shouldBe (readTime "19:52:23") (Right (Time 19 52 23))
    context "when provided with invalid input" $ do
      it "returns a parse error" $ do
        shouldBe (readTime "xx:12:4p") (Left "parse error")
      it "returns not valid time" $ do
        shouldBe (readTime "12:61:10") (Left "not valid time")
        shouldBe (readTime "25:23:10") (Left "not valid time")
        shouldBe (readTime "17:29:78") (Left "not valid time")
