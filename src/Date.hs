{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Date where

import Data.Attoparsec.ByteString
import Data.Attoparsec.ByteString.Char8
import Data.ByteString.Char8 (pack)
import Data.Either

-- | Pico y Placa restrictions are given by day of the weeks (Monday, Tuesday, etc)
data DayOfWeek
  = Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday
  | Saturday
  | Sunday
  deriving (Show, Eq)

-- | Internal representation of a date.
data Date = Date
  { month :: Int
  , day :: Int
  , year :: Int
  } deriving (Show, Eq)

-- | Parser for date
dateParser :: Parser Date
dateParser = do
  mm <- count 2 digit
  char '-'
  dd <- count 2 digit
  char '-'
  yyyy <- count 4 digit
  return $ Date (read mm) (read dd) (read yyyy)

-- | Month is valid
monthValidator :: Date -> Bool
monthValidator Date {..} = (month >= 1) && (month <= 12)

-- | Returns whether or not the year is leap
isLeap :: Date -> Bool
isLeap Date {..} =
  let isYearDiv4 = mod year 4 == 0
      isYearDiv100 = mod year 100 == 0
      isYearDiv400 = mod year 400 == 0
   in isYearDiv4 &&
      (not isYearDiv100 && not isYearDiv400 || isYearDiv100 && isYearDiv400)

-- | Validate February day number
febValidator :: Date -> Bool
febValidator date@Date {..} =
  if month == 2
    then if isLeap date
           then (day >= 1) && (day <= 29)
           else (day >= 1) && (day <= 28)
    else True

-- | Validate other months day number
otherMonthsValidator :: Date -> Bool
otherMonthsValidator Date {..} =
  if elem month [4, 6, 9, 11]
    then (day >= 1) && (day <= 30)
    else (day >= 1) && (day <= 31)

-- | Day is valid
dayValidator :: Date -> Bool
dayValidator date = febValidator date && otherMonthsValidator date

-- | Validate Date
dateValidator :: Date -> Either String Date
dateValidator date =
  if monthValidator date && dayValidator date
    then Right date
    else Left "not valid date"

-- | Reads date from a string (mm-dd-yyyy) if possible, otherwise returns parse error.
readDate :: String -> Either String Date
readDate rawDate =
  case parseOnly dateParser $ pack rawDate of
    Right x -> dateValidator x
    Left _ -> Left "parse error"

-- | Zeller's Rule
zellerRule :: Date -> Int
zellerRule date = k + div (13 * m - 1) 5 + d + (div d 4) + (div c 4) - (2 * c)
  where
    k = day date
    m = (+ 1) $ mod (month date + 9) 12
    d' = mod (year date) 100
    d =
      if (month date == 1) || (month date == 2)
        then pred d'
        else d'
    c = div (year date) 100

-- | Determines the day of week (Mon, Tue, etc) from a date.
dateToDayWeek :: Date -> DayOfWeek
dateToDayWeek date =
  case mod (zellerRule date) 7 of
    0 -> Sunday
    1 -> Monday
    2 -> Tuesday
    3 -> Wednesday
    4 -> Thursday
    5 -> Friday
    6 -> Saturday
