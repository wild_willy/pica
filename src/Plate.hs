module Plate where

-- | Internal representation for the last digit of a plate number.
data Digit
  = Zero
  | One
  | Two
  | Three
  | Four
  | Five
  | Six
  | Seven
  | Eight
  | Nine
  deriving (Show, Eq)

-- | Returns the last digit of a plate number.
lastDigit :: Int -> Digit
lastDigit plateNumber =
  case mod plateNumber 10 of
    0 -> Zero
    1 -> One
    2 -> Two
    3 -> Three
    4 -> Four
    5 -> Five
    6 -> Six
    7 -> Seven
    8 -> Eight
    9 -> Nine
