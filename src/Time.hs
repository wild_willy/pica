module Time where

import Data.Attoparsec.ByteString
import Data.Attoparsec.ByteString.Char8
import Data.ByteString.Char8 (pack)
import Data.Either

-- | Internal representation of time hh-mm-ss
data Time = Time
  { hour :: Int
  , minute :: Int
  , second :: Int
  } deriving (Show, Eq, Ord)

-- | Time parser
timeParser :: Parser Time
timeParser = do
  hh <- count 2 digit
  char ':'
  mm <- count 2 digit
  char ':'
  ss <- count 2 digit
  return $ Time (read hh) (read mm) (read ss)

-- | Validate Hour (0-24)
hourValidator :: Time -> Bool
hourValidator time =
  let h = hour time
   in (h >= 0) && (h <= 24)

-- | Validate minutes (0-60)
minValidator :: Time -> Bool
minValidator time =
  let m = minute time
   in (m >= 0) && (m <= 60)

-- | Validate seconds (0-60)
secValidator :: Time -> Bool
secValidator time =
  let s = second time
   in (s >= 0) && (s <= 60)

-- | Validate the time
timeValidator :: Time -> Either String Time
timeValidator time =
  let validators = [hourValidator, minValidator, secValidator]
      validation = and $ map (\f -> f time) validators
   in if validation
        then Right time
        else Left "not valid time"

-- | Reads the time from a String (hh:mm:ss).
readTime :: String -> Either String Time
readTime rawTime =
  case parseOnly timeParser $ pack rawTime of
    Right x -> timeValidator x
    Left _ -> Left "parse error"
