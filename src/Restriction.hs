module Restriction where

import Date
import Plate
import Time

-- | Checks whether or not the car violates the restriction (Quito Hardcoded rules)
checkPicoPlaca :: Digit -> Date -> Time -> Bool
checkPicoPlaca digit date time =
  let dayWeek = dateToDayWeek date
      timeRestriction1 = (time >= Time 07 00 00) && (time <= Time 09 30 00)
      timeRestriction2 = (time >= Time 16 00 00) && (time <= Time 19 30 00)
      rule1 = (dayWeek == Monday) && ((digit == One) || (digit == Two))
      rule2 = (dayWeek == Tuesday) && ((digit == Three) || (digit == Four))
      rule3 = (dayWeek == Wednesday) && ((digit == Five) || (digit == Six))
      rule4 = (dayWeek == Thursday) && ((digit == Seven) || (digit == Eight))
      rule5 = (dayWeek == Friday) && ((digit == Nine) || (digit == Zero))
   in (&&)
        (or [timeRestriction1, timeRestriction2])
        (or [rule1, rule2, rule3, rule4, rule5])
