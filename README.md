# pica
System to determine whether or not a vehicle is able to drive in accordance with the "Pico y Placa" restrictions from Quito city.
It uses Quito's Rules (Hardcoded):

Timeframe: 07:00 - 09:30  and 16:00 - 19:30
Rules: Mon 1 and 2 
       Tue 3 and 4
       Wed 5 and 6
       Thu 7 and 8
       Fri 9 and 0
